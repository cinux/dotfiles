config.load_autoconfig(False)
# keys
bindings = {
    #",d": "download-open",
    ",m": "spawn mpv '{url}'",
    ",M": "hint links spawn mpv '{hint-url}'",
    #",p": "spawn --userscript qute-pass --username-target secret --username-pattern 'user: (.+)' --dmenu-invocation 'dmenu -p credentials'",
    #",P": "spawn --userscript qute-pass --username-target secret --username-pattern 'user: (.+)' --dmenu-invocation 'dmenu -p password' --password-only",
    #",b": "config-cycle colors.webpage.bg '#1d2021' 'white'",
    #";I": "hint images download",
    #"<Ctrl-Shift-J>": "tab-move +",
    #"<Ctrl-Shift-K>": "tab-move -",
    #"M": "nop",
    #"co": "nop",
    #"<Shift-Escape>": "fake-key <Escape>",
    #"o": "set-cmd-text -s :open -s",
    #"O": "set-cmd-text -s :open -t -s",
}

for key, bind in bindings.items():
    config.bind(key, bind)
