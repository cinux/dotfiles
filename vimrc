"deactivate mouse
set mouse=
" set color
set number
syntax on
set tabstop=2

" from bit
set t_Co=256
set number
colorscheme koehler
set background=dark
set hidden
set history=100
filetype indent on
"set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
" set autoindent
set hlsearch
set ignorecase
set incsearch
set smartcase
set wildmenu
" disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
" disable page up/down
noremap <PageUp> <NOP>
noremap <PageDown> <NOP>
"set list
" for more visible cursor
"autocmd InsertEnter * set cul
"autocmd InsertLeave * set nocul
" add full file path to your existing statusline
set statusline=%F
set laststatus=2

call plug#begin()
Plug 'pearofducks/ansible-vim', { 'tag': '3.3' }
call plug#end()

let g:vimwiki_list = [{'path': '~/wiki', 'syntax': 'markdown', 'ext': '.md', 'path_html': 'site_html/', 'custom_wiki2html': 'vimwiki_markdown'}]

au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
let g:coc_filetype_map = {
  \ 'yaml.ansible': 'ansible',
  \ }
" run shellcheck on write
autocmd BufWritePost *.sh execute '!shellcheck %'
