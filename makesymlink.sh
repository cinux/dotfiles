#!/bin/bash
#
# Author: Cinux[90]
#
# This scipt create symlinks for each file under dotfiles
############################

set -o pipefail # exite incase of pipefailure
set -e # exite incase of failure
# set -x # for debug
# set -u # ignore unused variables

########## Variables
DOTFILEDIR=$(pwd)                    # dotfiles directory
BACKUPDIR="$HOME/.dotfiles_backup"            # old dotfiles backup directory
CONFIGDIR="config/qutebrowser config/mpv config/i3 config/wofi config/waybar config/rofi config/nvim"
BINDIR="bin"
LOCALSHAREDIR="local/share/qutebrowser"
ALLDIR="$BINDIR $CONFIGDIR $LOCALSHAREDIR"

# return list of files of $dir
getAllFiles() {
  if [ "$dir" == "." ]
  then
    find "$dir" -maxdepth 1 -type f ! -iname "README.md" ! -iname ".gitignore" ! -iname "MakeSymlink*.sh" -exec basename {} \;
  else
    find "$dir" -maxdepth 1 -type f
  fi
}

# create symlinks for 
# t = $target
# s = source
createSymlink() {
  echo "Create Symlink for $s to $t"
  if [ -d "$t" ] || [ -f "$t" ] && [ ! -L "$t" ]
  then
  	rm -rf "$t"
  elif [ -L "$t" ]
  then
	  echo "File a symlink, skipped"
	  return
  fi
  ln -s "$s" "$t"
}

backup() {
  echo "Start Backup"
	mkdir -p "$BACKUPDIR"
	for file in $files; do
    echo "Backup $file"
    if [ -f "$HOME/.$file" ]
    then
		  cp -rbLf "$HOME/.$file" "$BACKUPDIR/.$file"
    fi
	done
	for d in $ALLDIR; do
    echo "Backup $d"
	  s="$DOTFILEDIR/$d"
	  t="$HOME/.$d"
    if [ -d "$t" ]
    then
		  cp -rbLf "$t" "$BACKUPDIR"
    fi
	done
}

if ! git diff-index --quiet HEAD 
then
  echo "Unversioned changes detected! Please fix first."
  exit 1
fi

dir=.
files=$(getAllFiles)


# create backup
backup

# create symlink for files
echo "files: $files"
for file in $files; do
  s="$DOTFILEDIR/$file"
  t="$HOME/.$file"
  createSymlink
done


# create symlink for files
for d in $ALLDIR; do
  echo "Directory: $d"
  s="$DOTFILEDIR/$d"
  t="$HOME/.$d"
  createSymlink
done
