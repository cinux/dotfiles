#!/bin/sh
## xrandr: display setup
## external screen connected

## Variables
# T480 display
DISPLAY_LAPTOP="eDP-1"
# Get external screen name
EXTERNAL="$(xrandr | awk '/ connected/ && !/'$DISPLAY_LAPTOP'/ {print $1; exit}')"
# Get external screen maximum resolution/hertz (Hz)
MAXMODE="$(xrandr | grep --after-context=1 $EXTERNAL | tail -1)"
RESOLUTION="$(echo $MAXMODE | grep -Po '[0-9]+x[0-9]+')"
HZ="$(echo $MAXMODE | grep -Eo '\b[0-9]+(\.[0-9]+)?\b' | sort -g | tail -1)"

## Set screen
# Reset to defaults
xrandr -s 0
if [ -n "$EXTERNAL" ]; then
	# External screen on
	xrandr --output "$EXTERNAL" --mode "$RESOLUTION" --rate "$HZ" || exit 1
	# Internal screen off
	xrandr --output "$DISPLAY_LAPTOP" --off
else
	# Fallback
	# Internal screen on
	xrandr --output "$DISPLAY_LAPTOP" --auto --mode 1920x1080
fi
