# dotfiles
Here you will find all my used dotfiles for every application which I use or have used in the pass.

## Usage
Clone the repo and execute makesymlink.sh

```
git clone https://codeberg.org/cinux/dotfiles ~/.dotfiles
cd ~/.dotfiles/ && bash makesymlink.sh && cd -
```

## Extending

To extend the list of configuration files or folders go on as following:

**File**
Setup your file in the root, push it to your repository and reexecute makesymlink.sh

**Directory**
Setup the new folder in the root, open makesymlink.sh, create a new variable which reflect your folder and subfolders.
Extend the ALLDIR variable by adding the variable from the line above.
Update your repository and reecexcute makesymlink.sh

## NeoVim

### Installation

Dependend on the package manager install neovim.
As I also use [lazyvim](https://www.lazyvim.org/) following requiremends need to fullfit:

- Neovim >= 0.9.0
- Git >= 2.19.0
- [Nerd Font](https://www.nerdfonts.com/) (optional)
- a C compiler (should have nearly everyone :) )
- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim) (option and not used here)
- a terminal that support true color and undercurl

Requirements can be found [here](https://www.lazyvim.org/) again.

In my case, I use manjaro, i need to execute:

**Note: make is required**
```bash
pacman -S neovim make ripgrep fd lazygit
```

### First start

The first start of neovim will show you the lazyvim "window". It will download all necessary plugins and enable the features.
Afterwards execute `:LazyHealth` to install any additional plugins and stuff like that.

For the first time using Neovim execute following commands after open up it the first time:

```
:PlugInstall
:CocInstall @yaegassy/coc-ansible
```
