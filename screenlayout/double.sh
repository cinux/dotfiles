#!/bin/sh
xrandr --output HDMI-0 --mode 1920x1200 --pos 0x0 --rotate right --output DP-0 --off --output DP-1 --off --output DP-2 --mode 1920x1080 --pos 1200x420 --rate 144 --primary --rotate normal --output DP-3 --off --output DP-4 --off --output DP-5 --off
