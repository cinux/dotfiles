#
# ~/.bashrc
#

[ -n $BASH_VERSION ] || return 0

[[ $- != *i* ]] && return 0

# Binds
## Ignore Case for Tab Completion
bind "set completion-ignore-case on"
## Scroll through tab completion
# bind "TAB":"menu-complete"

# shopts
shopt -s autocd     # cd wihtout command cd
shopt -s checkjobs  # check if background job is running before exiting
shopt -s checkwinsize # check window size and resize
shopt -s cdspell # autocorrect minor spelling errors
shopt -s dirspell # bash attempts spelling correction
shopt -s dotglob # include leading dot-files/dirs

shopt -s cmdhist       # save multiline commands in one history line
shopt -s histappend # append instead of overwrite

# export HISTTIMEFORMAT="%F %T "
export HISTSIZE=10000
export HISTFILESIZE=10000
export HISTCONTROL="ignorespace:ignoredups"
export HISTIGNORE="history:ls"
export TERM=xterm-256color
export EDITOR=vim

[[ -f ~/.extend.bashrc ]] && . ~/.extend.bashrc

[[ -f ~/.bashrc_aliases ]] && . ~/.bashrc_aliases

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

function ii()   # get current host related info
{
  echo -e "\nYou are logged on ${RED}$HOST"
  echo -e "\nAdditionnal information:$NC " ; uname -a
  echo -e "\n${RED}Users logged on:$NC " ; w -h
  echo -e "\n${RED}Current date :$NC " ; date
  echo -e "\n${RED}Machine stats :$NC " ; uptime
  echo -e "\n${RED}Memory stats :$NC " ; free -m
  my_ip 2>&- ;
  echo -e "\n${RED}Local IP Address :$NC" ; echo ${MY_IP:-"Not connected"}
  echo -e "\n${RED}ISP Address :$NC" ; echo ${MY_ISP:-"Not connected"}
  echo
}

function ddiff()
{
  sdiff <(find $1 -printf '%f\n' | sort) <(find $2 -printf '%f\n' | sort)
}

function swfn()         
{
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE && mv "$2" "$1" && mv $TMPFILE "$2"
}

# [ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
	    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
	            eval "$("$BASE16_SHELL/profile_helper.sh")"
eval $(ssh-agent)
